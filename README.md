# jackett

[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

Use many torrent trackers with software that supports torznab/potato feeds

https://github.com/Jackett/Jackett
<br><br>
How to clone this repository:
```
git clone https://gitlab.com/azul4/content/torrent-programs/jackett.git
```
<br>
